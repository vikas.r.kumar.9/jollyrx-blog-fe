# Stage 0, "build-stage", Build using Node.js, Angular CLI. To build and compile the frontend.
FROM node:lts-alpine as build-stage

WORKDIR /app

COPY package.json  ./

RUN npm install

COPY . .

EXPOSE 80

CMD ["npm","run","dev"]

