/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    loader: "default",
    domains: ["localhost",'18.223.205.130',"strapi-jollyrx-assets.s3.amazonaws.com"],
  },
}

module.exports = nextConfig
